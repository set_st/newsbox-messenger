﻿using System;

namespace newsbox_messenger
{
    class Program
    {
        static void Main(string[] args)
        {
            string host;

            host = "127.0.0.1";
            
            System.Threading.Tasks.Task.Run(() => new Server(host, 13000));

            Console.WriteLine("Server Started...!");
            Console.ReadKey();
        }
    }
}
