﻿using System;
using System.Net.Sockets;
using System.Threading;

namespace newsbox_messenger
{
    class PingTimer : IDisposable
    {

        static TimerCallback tm = new TimerCallback(Client);
        static Timer _timer = new Timer(tm, null, 0, 100);
        static TcpClient _client;

        public PingTimer(int interval, TcpClient client)
        {
            _client = client;
            _timer.Change(0, interval);
        }

        static void Client(object obj)
        {
            try
            {
                var stream = _client.GetStream();
                string str = "Ping";
                Byte[] reply = System.Text.Encoding.ASCII.GetBytes(str);
                stream.Write(reply, 0, reply.Length);
            }
            catch (ObjectDisposedException e)
            {
                Console.WriteLine("Clent Exception: {0}", e.ToString());
                _client.Close();
                _timer.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.ToString());
                _client.Close();
                _timer.Dispose();
            }
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}
